# Development

# 3.3.3

- Updated annotations for methyltransferases mamA, mamB, and mamC(aka hsdM)
- Added in the mtb32a gene name synonym for Rv0125 (part of the M72 vaccine)

# 3.3.2

- Minor clean-ups of gene names and gene synonyms

# 3.3.1

- Fixed casing on sseC2
- Removed conflicting gene name assignment for Rv1636 (conflicted with Rv2316's)
- Removed mismatching name for Rv3669
- Broke up compound gene names into `gene` and `gene_synonym` fields
- Fixed issue with propagation of gene qualifier for Rv3364c into the final gbk and gff outputs

# 3.3

- switched to Refseq's start positions for PE_PGRS49 and PE_PGRS56 (#75).
  Mycobrowser's start positions are not known start codons.
- reduced confidence of Rv0264c and Rv0263c's functional annotation
- added pseudogene tags where applicable.
- added gene name fields where they were previously missing.

# 3.2

- More stringent validation of inferences from I-TASSER structural
predictions using manual structural curation and comparison to HHPred.
- Correct the strand for genes for which it was incorrectly imported
from TubercuList

# 3.1 - 2018 submission to biorxiv

Annotations based on TubercuList R27 supplemented with
- manual curation of literature published between
  2010-01-01 and 2017-06-30 for genes in the "hypothetical" and
  "unknown" functional categories
- inferences based on structural modeling from I-TASSER
