NAME = H37Rv
ACCESSION = NC_000962.3


all: $(NAME).gff $(NAME).gbf $(NAME).gtf

# create a VEP-compatible gtf annotation.
# Our particular gff files haven't been adequate.
%.gtf: %.gff
	agat_convert_sp_gff2gtf.pl --gff $< --gtf_version 3 -o $@

# create gff from gbk
$(NAME).gff: $(NAME).gbf
	seqret $< $@ -feature -osf gff

%.embl: %.gbf
	seqret $< $@ -feature -osf embl

#create gbk from tbl
$(NAME).gbf: template.sbt $(NAME).tbl $(NAME).fsa
	tbl2asn -t $< -p. -Vb \
	-n "Mycobacterium tuberculosis" \
	-j "[gcode=11]"

$(NAME).tbl: $(wildcard features/*.tbl)
	echo ">Feature $(ACCESSION)" | cat - $^ \
	| sed -r '/\t{4}ncnote\t/d' > $@

entrez-efetch = https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi
$(NAME).fsa:
	wget -O $@ "$(entrez-efetch)?db=nuccore&id=$(ACCESSION)&rettype=fasta"
