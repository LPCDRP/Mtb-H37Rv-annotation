Submit-block ::= {
  contact {
    contact {
      name name {
        last "",
        first "",
        middle "",
        initials "",
        suffix "",
        title ""
      },
      affil std {
        affil "San Diego State University",
        div "Laboratory for Pathogenesis of Clinical Drug Resistance and 
 Persistence",
        city "San Diego",
        sub "CA",
        country "USA",
        street "6367 Alvarado Court, #206",
        email "lpcdrp@sdsu.edu",
        postal-code "92120"
      }
    }
  },
  cit {
    authors {
      names std {
        {
          name name {
            last "",
            first "",
            middle "",
            initials "",
            suffix "",
            title ""
          }
        }
      },
      affil std {
        affil "San Diego State University",
        div "Laboratory for Pathogenesis of Clinical Drug Resistance and 
 Persistence",
        city "San Diego",
        sub "CA",
        country "USA",
        street "6367 Alvarado Court, #206",
        postal-code "92120"
      }
    }
  },
  subtype new
}
Seqdesc ::= pub {
  pub {
    gen {
      cit "unpublished",
      authors {
        names std {
          {
            name name {
              last "",
              first "",
              middle "",
              initials "",
              suffix "",
              title ""
            }
          }
        }
      },
      title "H37Rv"
    }
  }
}
Seqdesc ::= user {
  type str "Submission",
  data {
    {
      label str "AdditionalComment",
      data str "Submission Title:None"
    }
  }
}
