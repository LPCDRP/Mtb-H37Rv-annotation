# Mycobacterium tuberculosis H37Rv Annotation

The genome annotation here follows the [DDBJ/ENA/GenBank Feature Table Definition](http://www.insdc.org/files/feature_table.html), but each feature is broken out into a separate file.
This separation allows for an easy way to track the history of each feature's annotation.

# Downloading GenBank or GFF annotation file

Annotation files for the entire H37Rv genome as reported in trhe associated [mSystems manuscript](https://journals.asm.org/doi/10.1128/mSystems.00673-21) can be downloaded in GFF or GenBank format from Release 3.2 on the [Releases page](https://gitlab.com/LPCDRP/Mtb-H37Rv-annotation/-/releases) 

# Generating a GenBank annotation file

Simply run

```
make
```

to generate the GFF and GenBank files.
[tbl2asn](https://www.ncbi.nlm.nih.gov/genbank/tbl2asn2/) is required for both.
[seqret](https://www.ebi.ac.uk/seqdb/confluence/display/THD/EMBOSS+seqret) is required to produce the GFF output.
If you'd only like the GenBank file and don't have `seqret`, simply run

```
make H37Rv.gbf
```

# How to cite

If you use genome annotations from this resource, please cite our [associated publication in mSystems](https://journals.asm.org/doi/10.1128/mSystems.00673-21). For literature-derived annotations, please also cite the primary source from which the functional annotation was curated. PubMed IDs (PMID) are listed for each annotation in the .tbl files for each feature, and in the GFF and GenBank annotation file. 

# Where to find structural information 

Predicted 3D structure, functional links, structural homologs, and various quality information can be found at our [lab website](https://tuberculosis.sdsu.edu/structures/)   

# How to look up annotation information for a single gene

Annotations for each gene are located under https://lpcdrp.gitlab.io/Mtb-H37Rv-annotation/`<`ID`>`, where the ID can be either the locus tag, gene name, or a gene synonym. For example, if you were interested in the function of Rv1021, its annotation(s) can be found at https://lpcdrp.gitlab.io/Mtb-H37Rv-annotation/Rv1021 or https://lpcdrp.gitlab.io/Mtb-H37Rv-annotation/mazG.
Similarly, the annotation for Rv1169 can be accessed as  https://lpcdrp.gitlab.io/Mtb-H37Rv-annotation/PE11 in addition to the primary gene name  https://lpcdrp.gitlab.io/Mtb-H37Rv-annotation/lipX.

## Additional Notes

A `template.sbt` is used to generate the GenBank file.
A dummy file is included here, but a new one can be prepared from <https://submit.ncbi.nlm.nih.gov/genbank/template/submission/>.
